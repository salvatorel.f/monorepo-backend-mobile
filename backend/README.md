For this scenario i decided to use Serverless:
> The main idea behind the serverless is to reduce time, complexity and, ultimately, cost of running a reliable and scalable server for a modern application. 
> The main concept of FaaS is that application logic (or part of it) can be expressed as a set of functions run independently only when the need arises.
> Serverless architectures offer greater scalability, more flexibility, and quicker time to release, all at a reduced cost.


![architecture](/doc/img/overview.jpg)



The api gatway requires an ApiKey, provided as output from the serverless deployment.


**Deployment Requirements:**

* AWS account.
* Serverless framework.
* Deploy with the following command:
```
sls deploy --region eu-central-1
```
  

**Test requirements**

* Mocha
* Chai
* Lambda Tester


**To run the tests**
```
# [executed in the root folder,backend]
 npm install
 mocha
```


You should be able to see the following:

```
  lambda connectivity test topTen
    ✓ Fetch data and produce 200 as statusCode for topTen: (251ms)

  lambda connectivity test topTen with garden
    ✓ Fetch data and produce 200 as statusCode for topTen: (257ms)

  groupby test
    ✓ successful group array:

  sortMapBySize test
    ✓ successful sorts maps:


  4 passing (514ms)
```


