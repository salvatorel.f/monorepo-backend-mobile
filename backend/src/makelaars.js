'use strict';
const http = require('https')

module.exports.topten = async event => {
    return TopTenRequest('/feeds/Aanbod.svc/json/ac1b0b1572524640a0ecc54de453ea9f/?type=koop&zo=/amsterdam/&page=1&pagesize=3000')
};

module.exports.topten.tuin = async event => {
    return TopTenRequest('/feeds/Aanbod.svc/json/ac1b0b1572524640a0ecc54de453ea9f/?type=koop&zo=/amsterdam/tuin/&page=1&pagesize=2000')
};
module.exports.groupBy = groupBy;
module.exports.sortMapBySizeAndResize = sortMapBySizeAndResize;

function groupBy(array, key) {
    return array.reduce((result, currentValue) => {
        (result[currentValue[key]] = result[currentValue[key]] || []).push(
            currentValue
        );
        return result;
    }, {});
}

function sortMapBySizeAndResize(mapObject) {
    return Object.keys(mapObject)
        .map(function (k) {
            return { key: k, value: mapObject[k] };
        })
        .sort(function (a, b) {
            return b.value.length - a.value.length;
        })
        .slice(0,10)
}

function TopTenRequest(path) {
    const options = {
        host: 'partnerapi.funda.nl',
        port: 443,
        path: path,
        method: 'GET'
    }

    return new Promise((resolve, reject) => {
        const req = http.request(options, (res) => {
            if (res.statusCode < 200 || res.statusCode >= 300) {
                return reject(new Error('statusCode=' + res.statusCode));
            }
            var body = [];
            var sortedAndResizedBody = [];

            res.on('data', function (chunk) {
                body.push(chunk);
            });
            res.on('end', function () {
                try {
                    body = JSON.parse(Buffer.concat(body).toString());
                    var groupedBody = groupBy(body.Objects, 'MakelaarId')

                    sortedAndResizedBody = sortMapBySizeAndResize(groupedBody)

                } catch (e) {
                    reject(e);
                }
                resolve(sortedAndResizedBody);
            });
        });
        req.on('error', (e) => {
            reject(e.message);
        });
        req.end();
    }).then((data) => {
        const response = {
            statusCode: 200,
            body: JSON.stringify(data),
        };
        return response;
    });
}

