
'use strict';
var assert = require('assert');
var expect = require('chai').expect;
var LambdaTester = require('lambda-tester');

var myLambda = require('../src/makelaars.js');

const stubArray =
    [{ "MakelaarId": 2323, "MakelaarNaam": "MAKELAAR 2323" },
    { "MakelaarId": 2345446, "MakelaarNaam": "MAKELAAR 2345446" },
    { "MakelaarId": 2345446, "MakelaarNaam": "MAKELAAR 2345446" },
    { "MakelaarId": 2345446, "MakelaarNaam": "MAKELAAR 2345446" },
    { "MakelaarId": 1111, "MakelaarNaam": "MAKELAAR 1111" },
    { "MakelaarId": 11112, "MakelaarNaam": "MAKELAAR 11112" },
    { "MakelaarId": 11113, "MakelaarNaam": "MAKELAAR 11113" },
    { "MakelaarId": 11114, "MakelaarNaam": "MAKELAAR 11114" },
    { "MakelaarId": 11115, "MakelaarNaam": "MAKELAAR 11115" },
    { "MakelaarId": 11116, "MakelaarNaam": "MAKELAAR 11116" },
    { "MakelaarId": 11117, "MakelaarNaam": "MAKELAAR 11117" },
    { "MakelaarId": 11118, "MakelaarNaam": "MAKELAAR 11118" },
    { "MakelaarId": 11119, "MakelaarNaam": "MAKELAAR 11119" },
    { "MakelaarId": 111110, "MakelaarNaam": "MAKELAAR 111110" },
    { "MakelaarId": 111111, "MakelaarNaam": "MAKELAAR 111111" },
    { "MakelaarId": 111112, "MakelaarNaam": "MAKELAAR 111112" },
    { "MakelaarId": 111113, "MakelaarNaam": "MAKELAAR 111113" },
    { "MakelaarId": 1111, "MakelaarNaam": "MAKELAAR 1111" }]

describe('lambda connectivity integration test topTen', function () {
    this.enableTimeouts(false)
    it(`Fetch data and produce 200 as statusCode for topTen:`, function () {
        return LambdaTester(myLambda.topten)
            .event({})
            .expectResult((result) => {
                console.log(result.body)
                expect(result.statusCode).to.equal(200)
            });
    });
});

describe('lambda connectivity integration test topTen with garden', function () {
    this.enableTimeouts(false)
    it(`Fetch data and produce 200 as statusCode for topTen:`, function () {
        return LambdaTester(myLambda.topten.tuin)
            .event({})
            .expectResult((result) => {
                expect(result.statusCode).to.equal(200)
            });;
    });
});

describe('groupby test', function () {

    it(`successful group array:`, function () {
        let groupedArray = myLambda.groupBy(stubArray, 'MakelaarId')
        assert.equal(2, groupedArray['1111'].length)
        assert.equal(1, groupedArray['2323'].length)
        assert.equal(3, groupedArray['2345446'].length)
    });
});

describe('sortMapBySizeAndResize test', function () {
    it(`successful sorts maps:`, function () {
        let sortedMap = myLambda.sortMapBySizeAndResize(myLambda.groupBy(stubArray, 'MakelaarId'))
        assert.equal('2345446', Object.values(sortedMap)[0].key)
        assert.equal('3', Object.values(sortedMap)[0].value.length)
        assert('3', Object.values(sortedMap)[0].value.length)
        assert(true, Object.values(sortedMap)[0].value.length > Object.values(sortedMap)[1].value.length)
        assert(true, Object.values(sortedMap)[1].value.length > Object.values(sortedMap)[2].value.length)
    });

    it(`successful resize maps:`, function () {
        let sortedMap = myLambda.sortMapBySizeAndResize(myLambda.groupBy(stubArray, 'MakelaarId'))
        console.log(Object.values(sortedMap).length)
        console.log(Object.values(sortedMap))
        assert.equal(10, Object.values(sortedMap).length)

    });
});