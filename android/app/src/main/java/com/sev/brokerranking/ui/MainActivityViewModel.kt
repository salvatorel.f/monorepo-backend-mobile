package com.sev.brokerranking.ui

import android.util.Log
import android.view.View
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.sev.brokerranking.model.Broker
import com.sev.brokerranking.network.NetworkService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response

interface MainActivityCallback {
    fun onFetchBrokersSuccess(brokers: List<Broker>)
    fun onFetchBrokerFail()
}

class MainActivityViewModel(
    val networkService: NetworkService
) :
    ViewModel(), Callback<List<Broker>> {
    private val TAG = this.javaClass.canonicalName

    var isLoading: ObservableBoolean = ObservableBoolean(false)
    var isGardenSelected: ObservableBoolean = ObservableBoolean(false)
    var recyclerViewVisibility = ObservableField(View.VISIBLE)
    var callback: MainActivityCallback? = null

    fun onRefresh() {
        isLoading.set(true)
        if (isGardenSelected.get()) {
            networkService.getTopTenWithGarden().enqueue(this)
        } else {
            networkService.getTopTen().enqueue(this)
        }
    }

    override fun onFailure(call: Call<List<Broker>>, t: Throwable) {
        isLoading.set(false)
        recyclerViewVisibility.set(View.GONE)
        Log.e(TAG, "onFailure: " + t.message.toString())
        callback?.onFetchBrokerFail()

    }

    override fun onResponse(
        call: Call<List<Broker>>,
        response: Response<List<Broker>>
    ) {
        isLoading.set(false)
        if (response.code() >= 400 && response.code() < 599) {
            onFailure(call, HttpException(response))
            return
        }
        recyclerViewVisibility.set(View.VISIBLE)
        response.body()?.let {
            callback?.onFetchBrokersSuccess(it)
        }
    }

}