package com.sev.brokerranking.ui.shared

import android.annotation.SuppressLint
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.sev.brokerranking.R

@SuppressLint("Registered")//@Registered -> No need to register BaseActivity in Manifest.
open class BaseGenericErrorActivity : AppCompatActivity() {

    fun showGenericErrorDialog(title: String? = null, buttonText: String? = null) {
        val builder = AlertDialog.Builder(this@BaseGenericErrorActivity)
        builder.setMessage(title ?: getString(R.string.generic_error_message))
            .setCancelable(true)
            .setPositiveButton(buttonText ?: getString(R.string.ok)) { dialog, _ ->
                dialog?.dismiss()
            }.show()
    }
}