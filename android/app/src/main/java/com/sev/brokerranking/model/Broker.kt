package com.sev.brokerranking.model

data class Broker(val key: String, val value: List<BrokerDetail>)
