package com.sev.brokerranking.model

import com.google.gson.annotations.SerializedName

data class BrokerDetail(
    @SerializedName("MakelaarId")
    val brokerId: String,
    @SerializedName("MakelaarNaam")
    val brokerName: String
)
