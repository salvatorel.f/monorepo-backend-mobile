package com.sev.brokerranking.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.sev.brokerranking.R
import com.sev.brokerranking.databinding.BrokerListItemBinding
import com.sev.brokerranking.model.Broker


class BrokersAdapter(
    var brokers: MutableList<Broker> = mutableListOf<Broker>()
) :
    RecyclerView.Adapter<BrokersAdapter.BrokerHolder>() {

    lateinit var binding: BrokerListItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BrokerHolder {
        val inflater = LayoutInflater.from(parent.context)
        binding = DataBindingUtil.inflate(inflater, R.layout.broker_list_item, parent, false)
        return BrokerHolder(binding)
    }

    fun setData(brokers: List<Broker>) {
        if (!this.brokers.equals(brokers)) {
            this.brokers.clear()
            this.brokers.addAll(brokers)
            notifyDataSetChanged()
        }
    }

    override fun getItemCount() = brokers.size

    override fun onBindViewHolder(holder: BrokerHolder, position: Int) {
        holder.bind(brokers.get(position))
    }


    class BrokerHolder(val binding: BrokerListItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(broker: Broker) {
            binding.broker = broker
        }
    }
}