package com.sev.brokerranking.ui

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.sev.brokerranking.R
import com.sev.brokerranking.databinding.ActivityMainBinding
import com.sev.brokerranking.model.Broker
import com.sev.brokerranking.network.NetworkService
import com.sev.brokerranking.ui.shared.BaseGenericErrorActivity
import org.koin.android.ext.android.inject


class MainActivity : BaseGenericErrorActivity(), MainActivityCallback {

    private lateinit var brokersAdapter: BrokersAdapter
    private val networkService: NetworkService by inject()

    lateinit var binding: ActivityMainBinding
    lateinit var viewModel: MainActivityViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        //Not really needed since we're not handling screen rotation but cleaner implementation.
        viewModel = ViewModelProvider(this, MainActivityViewModelFactory(networkService)).get(
            MainActivityViewModel::class.java
        )
        viewModel.callback = this
        binding.viewModel = viewModel
        brokersAdapter = BrokersAdapter()
        binding.recyclerView.adapter = brokersAdapter
        binding.recyclerView.layoutManager = LinearLayoutManager(this@MainActivity)
    }

    override fun onResume() {
        super.onResume()
        binding.viewModel?.onRefresh()
    }

    override fun onFetchBrokerFail() {
        brokersAdapter.setData(emptyList())
        showGenericErrorDialog()
    }

    override fun onFetchBrokersSuccess(brokers: List<Broker>) {
        brokersAdapter.setData(brokers)
    }
}
