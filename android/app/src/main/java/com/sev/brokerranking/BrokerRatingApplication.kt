package com.sev.brokerranking

import android.app.Application
import com.sev.brokerranking.dependencies.Modules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class BrokerRatingApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@BrokerRatingApplication)
            modules(Modules().getModules())
        }
    }
}