package com.sev.brokerranking.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sev.brokerranking.network.NetworkService

class MainActivityViewModelFactory(val networkService: NetworkService) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainActivityViewModel::class.java)) {
            return MainActivityViewModel(networkService) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}