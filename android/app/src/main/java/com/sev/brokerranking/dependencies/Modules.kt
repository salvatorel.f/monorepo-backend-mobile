package com.sev.brokerranking.dependencies

import com.sev.brokerranking.network.NetworkClient
import org.koin.core.module.Module
import org.koin.dsl.module


class Modules {

    private var modules = ArrayList<Module>()

    val networkModule = module {
        single {
            NetworkClient().create()
        }

        modules.add(this)
    }

    fun getModules(): MutableList<Module> {
        return modules
    }
}