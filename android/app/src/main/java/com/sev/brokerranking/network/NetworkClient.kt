package com.sev.brokerranking.network

import com.sev.brokerranking.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class NetworkClient() {
    private lateinit var service: NetworkService
    val retrofitClient = RetrofitClient()


    fun create(): NetworkService {
        service = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(retrofitClient.httpClient.build())
            .build()
            .create(NetworkService::class.java)
        return service
    }
}