package com.sev.brokerranking.network

import com.sev.brokerranking.model.Broker
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers

interface NetworkService {

    @Headers("Content-Type: application/json;charset=UTF-8")
    @GET("makelaars/topten")
    fun getTopTen(): Call<List<Broker>>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @GET("makelaars/topten/tuin")
    fun getTopTenWithGarden(): Call<List<Broker>>

}