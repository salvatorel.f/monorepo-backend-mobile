package com.sev.brokerranking.network

import com.sev.brokerranking.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

class RetrofitClient : OkHttpClient() {
    var client: OkHttpClient
    var httpClient = Builder()
    private val API_KEY_NAME = "x-api-key"

    init {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        httpClient.connectTimeout(30, TimeUnit.SECONDS)
        httpClient.readTimeout(30, TimeUnit.SECONDS)
        httpClient.addNetworkInterceptor(logging)
        httpClient.addInterceptor {
            var request = it.request()
            request = request.newBuilder().addHeader(API_KEY_NAME, BuildConfig.API_KEY).build()
            it.proceed(request)
        }
        client = httpClient.build()
    }
}