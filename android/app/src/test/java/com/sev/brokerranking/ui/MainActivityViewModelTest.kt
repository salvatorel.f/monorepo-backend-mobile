package com.sev.brokerranking.ui

import android.util.Log
import android.view.View
import com.sev.brokerranking.model.Broker
import com.sev.brokerranking.network.NetworkService
import io.mockk.every
import io.mockk.mockkStatic
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import retrofit2.Call
import retrofit2.Response

class MainActivityViewModelTest {
    private lateinit var subject: MainActivityViewModel
    @Mock
    lateinit var mockNetworkService: NetworkService
    @Mock
    lateinit var mockCallback: MainActivityCallback
    @Mock
    lateinit var mockApiCallRespone: Call<List<Broker>>
    @Mock
    lateinit var mockRespone: Response<List<Broker>>

    @Before
    fun setup() {
        mockkStatic(Log::class)
        every { Log.e(any(), any()) } returns 0

        MockitoAnnotations.initMocks(this@MainActivityViewModelTest)
        subject = MainActivityViewModel(mockNetworkService)
        subject.callback = mockCallback
        `when`(mockNetworkService.getTopTen()).thenReturn(mockApiCallRespone)
        `when`(mockNetworkService.getTopTenWithGarden()).thenReturn(mockApiCallRespone)
    }

    private fun verifyOnFailureScenario() {
        assertFalse(subject.isLoading.get())
        assertEquals(View.GONE, subject.recyclerViewVisibility.get())
        verify(mockCallback, times(1)).onFetchBrokerFail()
    }

    @Test
    fun itShouldSetLoadingToTrueAndFetchGardenApiOnRefreshAndWithGardenEnabled() {
        //GIVEN
        subject.isGardenSelected.set(true)

        //WHEN
        subject.onRefresh()

        //THEN
        assertTrue(subject.isLoading.get())
        verify(mockNetworkService, times(1)).getTopTenWithGarden()
        verify(mockNetworkService, times(0)).getTopTen()
    }

    @Test
    fun itShouldSetLoadingToTrueAndFetchTopTenApiOnRefreshAndWithGardenDisabled() {

        subject.onRefresh()

        assertTrue(subject.isLoading.get())
        verify(mockNetworkService, times(0)).getTopTenWithGarden()
        verify(mockNetworkService, times(1)).getTopTen()
    }

    @Test
    fun itShouldSetLoadingToFalseSetRecyclerviewVisibilityToFalseAndTriggerActivityCallbackOnFailure() {
        subject.onFailure(mockApiCallRespone, Throwable())

        verifyOnFailureScenario()
    }

    @Test
    fun itShouldSetLoadingToFalseAndTriggerOnFailureWhenStatusCodeNot200OnResponse() {
        `when`(mockRespone.code()).thenReturn(444)

        subject.onResponse(mockApiCallRespone, mockRespone)
        verifyOnFailureScenario()
    }


    @Test
    fun itShouldSetLoadingToFalseSetRecyclerViewVisibilityTriggerCallbackWhenStatusCode200OnResponse() {
        `when`(mockRespone.code()).thenReturn(200)
        `when`(mockRespone.body()).thenReturn(emptyList())

        subject.onResponse(mockApiCallRespone, mockRespone)

        assertEquals(View.VISIBLE, subject.recyclerViewVisibility.get())
        verify(mockCallback, times(1)).onFetchBrokersSuccess(ArgumentMatchers.anyList())

    }
}