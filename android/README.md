# Android mobile application

The application is written in **Kotlin**.

Architectural pattern: **MVVM** with **Databinding**

**Dependencies:**

* **Koin** for dependency injection.

* **Retrofit 2** as Rest Client.

* **Junit, Mockito and mockk** for testing


In order to buil the app successfully you need a file called:
apikey.properties.
This file needs to contain: API_KEY="API_KEY_HERE"
This is done to avoid to commit this key in this repo.

From an UI point of view the application consist of:

*  **Splash screen**:  
<img src="/doc/img/splash.png"  width="251" height="432%">


* **MainActivity**:
<img src="/doc/img/main_empty.png"  width="251" height="432%">  
<img src="/doc/img/main.png"  width="251" height="432%">

**Layout including:**
 
*  Custom bar with a switch to request Brokers which are representing properties with garden.
*  Swipe to refresh to reload data.
*  Recyclerview with Broker adapter to show the brokers within the list.
*  Default view in case of empty data or network disabled.

 The activity is extending a BaseGenericErrorActivity to show a dialog in case of error.
 
**MainActivityViewModel responsible for:**
*  React to the pull to refresh in order to fetch data.
*  Disable key interaction with view while fetching data.
*  Notifying activity once data retrieved or in case of error.
*  The MainActivityViewModel is fully tested.


