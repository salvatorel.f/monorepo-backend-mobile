# Assignment:

>  The funda API returns information about the objects that are listed on funda.nl which are for sale.   
>  Determine which makelaar's in Amsterdam have the most object listed for sale.   
>  Make a table of the top 10. Then do the same thing but only for objects with a tuin which are listed for sale.   
>  For the assignment you may write a program in any object oriented language of your choice and you may   
>  use any libraries that you find useful.


# Technical implementation
* Backend service able to fetch from a public api and manipulate the data. (AWS Serverless)

* Application that consume the manipulated data and show a top 10 ranking. (Android native application)


**Backend:**\
https://gitlab.com/salvatorel.f/monorepo-backend-mobile/-/blob/master/backend/README.md

**Mobile application:**\
https://gitlab.com/salvatorel.f/monorepo-backend-mobile/-/blob/master/android/README.md

